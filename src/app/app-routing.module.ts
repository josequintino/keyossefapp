import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'component-menuinferior',
    pathMatch: 'full'
  },
  {
    path: 'component-menuinferior',
    loadChildren: () => import('./component/component-menuinferior/component-menuinferior.module').then( m => m.ComponentMenuinferiorPageModule)
  },
  {
    path: 'screen-monitoramento',
    loadChildren: () => import('./module/screen-monitoramento/screen-monitoramento.module').then( m => m.ScreenMonitoramentoPageModule)
  },
  {
    path: 'screen-carteira',
    loadChildren: () => import('./module/screen-carteira/screen-carteira.module').then( m => m.ScreenCarteiraPageModule)
  },
  {
    path: 'screen-movimentacao',
    loadChildren: () => import('./module/screen-movimentacao/screen-movimentacao.module').then( m => m.ScreenMovimentacaoPageModule)
  },
  {
    path: 'screen-configuracao',
    loadChildren: () => import('./module/screen-configuracao/screen-configuracao.module').then( m => m.ScreenConfiguracaoPageModule)
  },
  {
    path: 'screen-transacao',
    loadChildren: () => import('./module/screen-transacao/screen-transacao.module').then( m => m.ScreenTransacaoPageModule)
  },
  {
    path: 'screen-pessoa',
    loadChildren: () => import('./module/screen-pessoa/screen-pessoa.module').then( m => m.ScreenPessoaPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
