import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScreenTransacaoPageRoutingModule } from './screen-transacao-routing.module';

import { ScreenTransacaoPage } from './screen-transacao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScreenTransacaoPageRoutingModule
  ],
  declarations: [ScreenTransacaoPage]
})
export class ScreenTransacaoPageModule {}
