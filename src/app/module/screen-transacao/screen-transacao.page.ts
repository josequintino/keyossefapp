import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-screen-transacao',
  templateUrl: './screen-transacao.page.html',
  styleUrls: ['./screen-transacao.page.scss'],
})
export class ScreenTransacaoPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() { }

  public async redirecionarTelaGerenciadorPessoa() {
    await this.router.navigate["screen-pessoa"];
  }

}
