import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScreenTransacaoPage } from './screen-transacao.page';

const routes: Routes = [
  {
    path: '',
    component: ScreenTransacaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenTransacaoPageRoutingModule {}
