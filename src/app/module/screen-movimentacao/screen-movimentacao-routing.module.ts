import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreenMovimentacaoPage } from './screen-movimentacao.page';

const routes: Routes = [
  {
    path: '',
    component: ScreenMovimentacaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenMovimentacaoPageRoutingModule {}
