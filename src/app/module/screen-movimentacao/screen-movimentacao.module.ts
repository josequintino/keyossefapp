import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScreenMovimentacaoPageRoutingModule } from './screen-movimentacao-routing.module';

import { ScreenMovimentacaoPage } from './screen-movimentacao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScreenMovimentacaoPageRoutingModule
  ],
  declarations: [ScreenMovimentacaoPage]
})
export class ScreenMovimentacaoPageModule {}
