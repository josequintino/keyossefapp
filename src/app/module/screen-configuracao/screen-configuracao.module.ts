import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScreenConfiguracaoPageRoutingModule } from './screen-configuracao-routing.module';

import { ScreenConfiguracaoPage } from './screen-configuracao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScreenConfiguracaoPageRoutingModule
  ],
  declarations: [ScreenConfiguracaoPage]
})
export class ScreenConfiguracaoPageModule {}
