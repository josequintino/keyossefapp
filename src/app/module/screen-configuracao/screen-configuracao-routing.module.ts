import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreenConfiguracaoPage } from './screen-configuracao.page';

const routes: Routes = [
  {
    path: '',
    component: ScreenConfiguracaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenConfiguracaoPageRoutingModule {}
