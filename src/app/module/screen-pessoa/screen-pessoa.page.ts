import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-screen-pessoa',
  templateUrl: './screen-pessoa.page.html',
  styleUrls: ['./screen-pessoa.page.scss'],
})
export class ScreenPessoaPage implements OnInit {

  // public pessoaList = ["José Quintinn", "Thereza das Graças Rodrigues", "Renata Rodrigues"];
  public pessoaList = ["Pessoa Sistema I", "Pessoa Sistema II", "Pessoa Sistema III"];

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.modalController.dismiss();
  }

}
