import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreenPessoaPage } from './screen-pessoa.page';

const routes: Routes = [
  {
    path: '',
    component: ScreenPessoaPage
  },
  {
    path: 'pessoa-cadastrar',
    loadChildren: () => import('./pessoa-cadastrar/pessoa-cadastrar.module').then( m => m.PessoaCadastrarPageModule)
  },
  {
    path: 'component-menuinferior',
    loadChildren: () => import('../../component/component-menuinferior/component-menuinferior.module').then( m => m.ComponentMenuinferiorPageModule)
  },
  {
    path: 'pessoa-editar',
    loadChildren: () => import('./pessoa-editar/pessoa-editar.module').then( m => m.PessoaEditarPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenPessoaPageRoutingModule {}
