import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScreenPessoaPageRoutingModule } from './screen-pessoa-routing.module';

import { ScreenPessoaPage } from './screen-pessoa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScreenPessoaPageRoutingModule
  ],
  declarations: [ScreenPessoaPage]
})
export class ScreenPessoaPageModule {}
