import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreenCarteiraPage } from './screen-carteira.page';

const routes: Routes = [
  {
    path: '',
    component: ScreenCarteiraPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenCarteiraPageRoutingModule {}
