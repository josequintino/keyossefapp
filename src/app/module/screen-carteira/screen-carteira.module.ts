import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScreenCarteiraPageRoutingModule } from './screen-carteira-routing.module';

import { ScreenCarteiraPage } from './screen-carteira.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScreenCarteiraPageRoutingModule
  ],
  declarations: [ScreenCarteiraPage]
})
export class ScreenCarteiraPageModule {}
