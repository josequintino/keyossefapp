import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScreenMonitoramentoPageRoutingModule } from './screen-monitoramento-routing.module';

import { ScreenMonitoramentoPage } from './screen-monitoramento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScreenMonitoramentoPageRoutingModule
  ],
  declarations: [ScreenMonitoramentoPage]
})
export class ScreenMonitoramentoPageModule {}
