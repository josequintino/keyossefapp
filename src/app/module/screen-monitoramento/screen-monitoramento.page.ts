import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screen-monitoramento',
  templateUrl: './screen-monitoramento.page.html',
  styleUrls: ['./screen-monitoramento.page.scss'],
})
export class ScreenMonitoramentoPage implements OnInit {

  public descricaoIcone: string;
  public isApresentarValoresFinanceiros: boolean = false;

  constructor() { }

  ngOnInit() { }

  public async habilitarExibicaoValoresFinanceiros() {
    if(!this.isApresentarValoresFinanceiros) {
      this.isApresentarValoresFinanceiros = true;
    } else {
      this.isApresentarValoresFinanceiros = false;
    }
  }

}
