import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScreenMonitoramentoPage } from './screen-monitoramento.page';

const routes: Routes = [
  {
    path: '',
    component: ScreenMonitoramentoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScreenMonitoramentoPageRoutingModule {}
