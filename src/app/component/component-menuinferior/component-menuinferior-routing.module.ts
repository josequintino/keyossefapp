import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentMenuinferiorPage } from './component-menuinferior.page';

const routes: Routes = [
  {
    path: '',
    component: ComponentMenuinferiorPage,
    children: [
      {
        path: "monitoramento",
        children: [
          {
            path: "",
            loadChildren: () => import("../../module/screen-monitoramento/screen-monitoramento.module").then(module => module.ScreenMonitoramentoPageModule)
          }
        ]
      },
      {
        path: "carteira",
        children: [
          {
            path: "",
            loadChildren: () => import("../../module/screen-carteira/screen-carteira.module").then(module => module.ScreenCarteiraPageModule)
          }
        ]
      },
      {
        path: "movimentacao",
        children: [
          {
            path: "",
            loadChildren: () => import("../../module/screen-movimentacao/screen-movimentacao.module").then(module => module.ScreenMovimentacaoPageModule)
          }
        ]
      },
      {
        path: "configuracao",
        children: [
          {
            path: "",
            loadChildren: () => import("../../module/screen-configuracao/screen-configuracao.module").then(module => module.ScreenConfiguracaoPageModule)
          }
        ]
      },
      {
        path: "transacao",
        children: [
          {
            path: "",
            loadChildren: () => import("../../module/screen-transacao/screen-transacao.module").then(module => module.ScreenTransacaoPageModule)
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComponentMenuinferiorPageRoutingModule {}
