import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComponentMenuinferiorPage } from './component-menuinferior.page';

describe('ComponentMenuinferiorPage', () => {
  let component: ComponentMenuinferiorPage;
  let fixture: ComponentFixture<ComponentMenuinferiorPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentMenuinferiorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ComponentMenuinferiorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
