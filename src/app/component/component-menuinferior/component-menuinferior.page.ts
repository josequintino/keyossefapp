import { ScreenTransacaoPage } from './../../module/screen-transacao/screen-transacao.page';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ScreenTransacaoPageModule } from 'src/app/module/screen-transacao/screen-transacao.module';

@Component({
  selector: 'app-component-menuinferior',
  templateUrl: './component-menuinferior.page.html',
  styleUrls: ['./component-menuinferior.page.scss'],
})
export class ComponentMenuinferiorPage implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }

  public async redirecionarTelaTransacoes() {
    const modal = await this.modalController.create({
      component: ScreenTransacaoPage,
      initialBreakpoint: 0.9,
      breakpoints: [0, 0.9, 1],
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
