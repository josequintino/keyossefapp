import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ComponentMenuinferiorPageRoutingModule } from './component-menuinferior-routing.module';

import { ComponentMenuinferiorPage } from './component-menuinferior.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentMenuinferiorPageRoutingModule
  ],
  declarations: [ComponentMenuinferiorPage]
})
export class ComponentMenuinferiorPageModule {}
